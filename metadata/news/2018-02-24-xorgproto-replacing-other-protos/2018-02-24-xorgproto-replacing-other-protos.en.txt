Title: x11-proto/xorgproto now replaces most other x11-proto packages
Author: Rasmus Thomsen <cogitri@exherbo.org>
Content-Type: text/plain
Posted: 2018-02-24
Revision: 1
News-Item-Format: 1.0

xorgproto has been added to ::x11. It is a collection of all x11-proto packages but
x11-proto/xcb-proto. Upstream decided that it's not worth it to keep them splitted,
as most changes affect most/all of them anyway. As such xorgproto ships the very same headers
the old ( split ) x11-proto packages did, so this change shouldn't lead to build failures and the likes.
Consequently all x11-proto packages but xcb-proto have been masked as pending-removal
and will be removed soon. ( Almost ) all packages have been migrated to xorgproto,
so you should be able to do the ( mandatory ) switch to it using:

    cave resolve world -c --permit-uninstall "x11-proto/*"

In case if you encouter a package which still depends on x11-proto packages other
than xcb-proto or xorgproto you're welcome to open a change replacing the offending
dependency with xorgproto.
