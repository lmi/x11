# Copyright 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure src_install

SUMMARY="Qt Cross-platform application framework: QtFeedback"
DESCRIPTION="A C++ API enabling a client to control and provide tactile and
audio feedback to the user. The feedback is in response to user actions. For
example, touching an onscreen button. Control of the feedback involves control
of the vibration of the device, when a vibrator is used, or the piezo feedback
from the screen."

LICENCES+="
    GPL-2
"
SLOT="5"
MYOPTIONS="examples"

DEPENDENCIES="
    build+run:
        x11-libs/qtbase:${SLOT}[gui]
        x11-libs/qtdeclarative:${SLOT}
        x11-libs/qtmultimedia:${SLOT}
"

qtfeedback_src_configure() {
    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    qmake_src_configure
}

qtfeedback_src_install() {
    qt_src_install

    # Remove broken cmake file, causes a "populate_Feedback_plugin_properties
    # Macro invoked with incorrect arguments" error
    edo rm "${IMAGE}"/usr/$(exhost --target)/lib/cmake/Qt5Feedback/Qt5Feedback_.cmake
}

