# Copyright 2017-2018, 2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt cmake [ ninja=true ]

export_exlib_phases src_compile src_install

SUMMARY="Qt Cross-platform application framework: QtSensors"
DESCRIPTION="
The Qt Sensors API provides access to sensor hardware via QML and C++
interfaces. The Qt Sensors API also provides a motion gesture
recognition API for devices."

LICENCES="FDL-1.3 GPL-2 GPL-3 LGPL-3"
MYOPTIONS="
    examples
    qml [[ description = [ Support for QtQuick and the QML language ] ]]
"

DEPENDENCIES="
    build+run:
        x11-libs/qtbase:${SLOT}[>=${PV}]
        examples? ( x11-libs/qtsvg:${SLOT}[>=${PV}] )
        qml? ( x11-libs/qtdeclarative:${SLOT}[>=${PV}] )
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_DISABLE_FIND_PACKAGE_Sensorfw:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'examples QT_BUILD_EXAMPLES'
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'examples Qt6Svg'
    'qml Qt6Qml'
    'qml Qt6Quick'
    'qml Qt6QuickTest'
)

qtsensors-6_src_compile() {
    cmake_src_compile

    option doc && eninja docs
}

qtsensors-6_src_install() {
    cmake_src_install

    option doc && DESTDIR="${IMAGE}" eninja install_docs
}

