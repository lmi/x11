# Copyright 2018 Danilo Spinella <danyspin97@protonmail.com>
# Copyright 2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user='assimp' tag=v${PV} ] cmake

SUMMARY="Official Open Asset Import Library"
DESCRIPTION="
A library to import and export various 3d-model-formats including
scene-post-processing to generate missing render data."

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        sys-libs/zlib
"
# TODO: Unbundle more deps, eg. utfcpp, gtest, clipper, pugixml, stb

# TODO: Fails 3 tests out of 565
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DASSIMP_ASAN=OFF
    # doc build is broken between Doxyfile and cmake targets
    -DASSIMP_BUILD_DOCS=OFF
    # Need some additional dependencies
    -DASSIMP_BUILD_SAMPLES=OFF
    -DASSIMP_BUILD_ZLIB=OFF
    # Breaks building x11-libs/qtquick3d, currently the only consumer
    #-DASSIMP_DOUBLE_PRECISION=ON
    # "Enable Hunter package manager support"
    -DASSIMP_HUNTER_ENABLED=OFF
    -DASSIMP_IGNORE_GIT_HASH=ON
    -DASSIMP_OPT_BUILD_PACKAGES=OFF
    -DASSIMP_UBSAN=OFF
    -DASSIMP_WARNINGS_AS_ERRORS=OFF
)

CMAKE_SRC_CONFIGURE_TESTS=( '-DASSIMP_BUILD_TESTS=ON -DASSIMP_BUILD_TESTS=OFF' )

src_prepare() {
    cmake_src_prepare

    # Just to make sure to not include the bundled version
    edo rm -r contrib/zlib
}

src_test() {
    edo bin/unit
}

